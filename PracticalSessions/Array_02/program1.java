// Take n integer values from user and insert the values and find the sum of all elements in array


import java.io.*;
import java .util.*;
class SumOfElements{
       	public static void main(String[]args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size of Array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter elements in array");
		for(int i =0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}
		int sum = 0;
		for(int i=0;i<arr.length;i++){
			sum = sum + arr[i];
		}
		System.out.println("Sum of array = " +sum);
	}
}


