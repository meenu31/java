// WAP to search a specific element from an array and return its index

import java.io.*;
class search{
	public static void main(String[]args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int [size];

		System.out.println("Enter elements in array");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter search element");
		int search = Integer.parseInt(br.readLine());

		for(int i=0;i<arr.length;i++){
			if(arr[i] == search){

				System.out.println("Elemnet found at index = "+i);
			}
		}
	}
}	


