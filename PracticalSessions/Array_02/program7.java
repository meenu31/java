// Find common elements between two arrays

import java.io.*;
class CommonElement{
	public static void main(String[]args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");
		int size = Integer.parseInt(br.readLine());

		int arr1[] = new int [size];
		
		System.out.println("Enter elements in array-1");

		for(int i=0;i<arr1.length;i++){
		arr1[i]= Integer.parseInt(br.readLine());
		}

		int arr2[] = new int [size];
		
		System.out.println("Enter elements in array-2");

		for(int i=0;i<arr2.length;i++){
			arr2[i] = Integer.parseInt(br.readLine());
		}

		for(int i=0;i<arr1.length;i++){

			for(int j=0;j<arr2.length;j++){

				if(arr1[i] == arr2[j]){

					System.out.println("Common elements between arrays are: " + arr1[i]);
				}
			}
		}
	}
}


	
