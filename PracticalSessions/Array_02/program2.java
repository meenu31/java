// WAP to find number of even and odd integers in a given array 

import java.io.*;
import java.util.*;

class EvenOddCount{
	public static void main(String[]args)throws IOException{

		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter size of array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter elements in array");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		int evencount=0, oddcount =0;

		for(int i=0;i<arr.length;i++){
			
			if(arr[i] % 2 == 0){
				evencount++;
			}else{
				oddcount++;
			}
		}
		System.out.println("Number of even numbers = " + evencount);
		System.out.println("Number of odd numbers = " + oddcount);
	}
}

