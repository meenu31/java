// User will enter 2 numbers if both the numbers are positve multiply them and provide to switch case to verify the number is even or odd. If the user enter any negative     number program should terminate by saying "Sorry negative numbers not allowed".


import java.io.*;
class MultNum{
	public static void main(String[]args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number1 = " );
		int num1 = Integer.parseInt(br.readLine());

		System.out.println("Enter number2 = " );
		int num2 = Integer.parseInt(br.readLine());

		if(num1<0||num2<0){
			System.out.println("Negative numbers not allowed");

		}else{
			switch((num1*num2)%2){

				case 0: 
					System.out.println("Multiplication is even");
					break;
				
				case 1: 
					System.out.println("Multiplication is odd");
					break;
			}
		}
	}
}
