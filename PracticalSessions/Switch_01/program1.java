// Switch case
// Enter marks of 5 subjects. if all subject having above passing marks add them and provide switch case to print grades(first class second class).if a student get fail in any subject then your program should print "You failed in exam".


import java.io.*;
class Result{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int EnglishMarks;
		System.out.println("Enter English marks = ");
		EnglishMarks = Integer.parseInt(br.readLine());

		int MathMarks;
		System.out.println("Ente Math marks = ");
		MathMarks = Integer.parseInt(br.readLine());

		int HindiMarks;
		System.out.println("Enter Hindi marks = ");
		HindiMarks = Integer.parseInt(br.readLine());

		int ScienceMarks;
		System.out.println("Enter Science marks = ");
		ScienceMarks = Integer.parseInt(br.readLine());

		int HistoryMarks;
		System.out.println("Enter History marks = ");
		HistoryMarks = Integer.parseInt(br.readLine());

		if(EnglishMarks<35||MathMarks<35||HindiMarks<35||ScienceMarks<35||HistoryMarks<35){
			System.out.println("You failed exam");

		}else{
			double avg = (EnglishMarks+MathMarks+HindiMarks+ScienceMarks+HistoryMarks)/5;

			char ch;
			if(avg>75){
				ch = 'A';
			
			}else if(avg>60){
				ch = 'B';

			}else if(avg>50){
				ch = 'C';

			}else if(avg>40){
				ch = 'D';

			}else if(avg>=35){
			      ch ='E';

			}else {
			      ch ='F';
			}

			switch(ch){
				case 'A': 
					  System.out.println("First Class with Distinction");
					  break;

				case 'B': 
					  System.out.println("First Class");
					  break;

				case 'C': 
					  System.out.println("Second Class");
					  break;

				case 'D': 
					  System.out.println("Third Class");
					  break;

				case 'E': 
					  System.out.println("Pass");
					  break;

				case 'F': 
					  System.out.println("Fail");
					  break;
			}
		}
	}
}

