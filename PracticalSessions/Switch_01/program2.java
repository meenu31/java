// Write a program that asks user to enter an number from 0 to 5 and print its spelling. if the entered number is greater than 5 print entered number is greater than 5



import java.io.*;
class SpellNum{
	public static void main(String[]args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number = ");
		int num=Integer.parseInt(br.readLine());

		switch(num){
			case 0: 
				System.out.println("Zero");
				break;
			
			case 1: 
				System.out.println("One");
				break;

			case 2: 
				System.out.println("Two");
				break;

			case 3: 
				System.out.println("Three");
				break;

			case 4: 
				System.out.println("Four");
				break;

			case 5: 
				System.out.println("Five");
				break;

			default: 
				System.out.println("Number is greater than 5");
				break;
		}
	}
}

