// print product of even numbers only

import java.io.*;
import java.util.*;

class EvenProduct{
	public static void main(String[] args) throws IOException{
		BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter array size");
		int size =Integer.parseInt(br.readLine());

		int arr[] =  new int[size];

		System.out.println("Enter elements");
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
	}
	
	int product = 1;
		for(int i=0;i<size;i++){
			if(arr[i] % 2 == 0){
				product = arr[i] * product;
			}
		}
		System.out.println("product of even number is = " + product);
	}
}


