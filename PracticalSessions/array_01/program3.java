//odd index count

import java.util.*;
import java.io.*;

class OddIndexCount{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter elements");
		for(int i=0;i<size;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		int mult = 1;
		for(int i=0; i<size;i++){
			if(i% 2!=0){
			mult= mult*arr[i];
		}
		}
		System.out.println("sum of odd index: " + mult);
	}
}
	
