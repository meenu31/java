// to print the square of even digits of the given no


class SquareOfEvenNo{
	public static void main(String[]args){

		int digit = 942111423;

		while(digit!=0){
			int rem = digit % 10;

			if(rem % 2 == 0){
				System.out.println((rem*rem));
			}
			digit/=10;
		}
	}
}

