// Calculate profit and loss
// write a program to take the cost price and selling price and calculate its profit and loss

class ProfitLoss{
	public static void main(String[]args){

		int selling_price = 1200;
		int cost_price = 1000;
		
		if(selling_price>cost_price){
			System.out.println("Profit of"+(selling_price-cost_price));
		}else if(selling_price<cost_price){
			System.out.println("Loss of"+(cost_price-selling_price));
		}else{
			System.out.println("No Profit And No Loss");
		}
	}
}
