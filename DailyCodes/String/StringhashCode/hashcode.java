class HashCode{
	public static void main(String[] args){
		
		String str1 = "Meenakshi";
		String str2 = new String("Meenakshi");
		String str3 = "Meenakshi";
		String str4 = new String("Meenakshi");

		System.out.println(str1.hashCode());
		System.out.println(str2.hashCode());
		System.out.println(str3.hashCode());
		System.out.println(str4.hashCode());
	}
}
