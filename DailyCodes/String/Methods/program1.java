// Method : CompareTo 

import java.io.*;
class myCompareToDemo{

	static int myStrCompareTo(String str1, String str2){

		char arr1[] = str1.toCharArray();
		char arr2[] = str2.toCharArray();

		if(arr1.length==arr2.length){
			for(int i=0;i<arr1.length;i++){
				if(arr1[i]==arr2[i]){
					continue;
				}
				else{
					int difference = arr1[i] - arr2[i];
					return difference;
				}
			}
		}
		else{
			for(int i=0;i<arr2.length;i++){
				if(arr1[i]==arr2[i]){
					continue;
				}
				else{
					int difference = arr1[i]=arr2[i];
					return difference;
				}
			}
		}
		return 0;
	}

	public static void main(String [] meenakshi)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter First String:");
		String str1 = br.readLine();

		System.out.println("Enter Second String:");
		String str2 = br.readLine();

		int compare = myStrCompareTo(str1,str2);
		System.out.println(compare);
	}
}
