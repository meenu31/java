

class StringDem{
	public static void main(String[]args){

		String str1 = "Meenakshi";
		String str2 = new String("Meenakshi");

		char str3[] = {'m','e','e','n','u'};
		
		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
		
		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		System.out.println(System.identityHashCode(str3));
	}
}


