// Multiplication of digits of integer


class Multiplication{
	public static void main(String[]args){
		
		int n = 135;
		int mult = 1;

		while(n!=0){
			int rem = n % 10;
			mult = mult * rem;
			n = n/10;
		}
		System.out.println("Multiplication of digits is : " + mult);
	}
}
