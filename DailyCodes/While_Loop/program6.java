// Given ana integer n and print all its digits


class Integers{
	public static void main(String[]args){

		int n = 653;

		while(n!=0){
			int digit = n % 10;
			System.out.println(digit);
			n = n/10;
		}
	}
}
