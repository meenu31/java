// Divisible by 3 between 1 to 50


class Divisible{
	public static void main(String[]args){

		int i=3;
		int n=50;

		while(i<=50){
			if(i%3 ==0){
				System.out.println(i);
			}
			i++;
		}
	}
}
