// Reverse of integer n 


class ReverseInteger{
	public static void main(String[]args){

		int n = 6531;
		int num = 0;

		while(n!=0){
			int rem = n%10;
			num = num * 10 + rem;
			n = n/10;
		}
		System.out.println("Reverse number is : " + num);
	}
}
